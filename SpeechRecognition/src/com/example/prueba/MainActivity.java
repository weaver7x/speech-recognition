package com.example.prueba;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Actividad que muestra un bot�n para comenzar a reconocer audio y muestra el
 * resultado de dicho reconomiento.
 * 
 * @author Cristian TG
 * @since 2017-02-26
 * @version 1.0
 */
public final class MainActivity extends Activity {
	private String language = "en-US";
	private final static int SPEECH_RECOGNITION_CODE = 1;
	private TextView txtOutput;
	private Button btnMicrophone;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.txtOutput = (TextView) findViewById(R.id.txt_output);
		this.btnMicrophone = (Button) findViewById(R.id.btn_mic);
		this.btnMicrophone.setText(this.btnMicrophone.getText().toString()
				+ " " + this.language);
		this.btnMicrophone.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startSpeechToText();
			}
		});
	}

	/**
	 * Start speech to text intent. This opens up Google Speech Recognition API
	 * dialog box to listen the speech input.
	 */
	private void startSpeechToText() {
		final Intent intent = new Intent(
				RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, this.language);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
				RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
		intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 10);
		intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak something...");
		try {
			startActivityForResult(intent, SPEECH_RECOGNITION_CODE);
		} catch (ActivityNotFoundException a) {
			Toast.makeText(
					getApplicationContext(),
					"Sorry! Speech recognition is not supported in this device.",
					Toast.LENGTH_SHORT).show();
		}
	}

	@SuppressLint("InlinedApi")
	@Override
	/**
	 * Callback for speech recognition activity
	 */
	protected void onActivityResult(final int requestCode,
			final int resultCode, final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case SPEECH_RECOGNITION_CODE: {
			if (resultCode == RESULT_OK && null != data) {
				final ArrayList<String> result = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
				String text = result.toString();
				if (Build.VERSION.SDK_INT >= 14) {
					text = text + '\n';
					float[] gscores = data
							.getFloatArrayExtra(RecognizerIntent.EXTRA_CONFIDENCE_SCORES);
					for (int i = 0; i < gscores.length; i++) {
						text = text + gscores[i] + " - ";
					}
				}

				this.txtOutput.setText(text);
			}
			break;
		}
		}
	}
}